<?php

use App\Http\Controllers\routerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [routerController::class, 'home'])->name('home');
Route::get('/dashboard', [routerController::class, 'dashboard'])->name('dashboard');
Route::get('/register', [routerController::class, 'register'])->name('register');
Route::get('/login', [routerController::class, 'login'])->name('login');
Route::get('/shop', [routerController::class, 'shop'])->name('shop');
Route::get('/welcome', [routerController::class, 'welcome'])->name('welcome');
Route::get('/langganan', [routerController::class, 'langganan'])->name('langganan');
Route::get('/paket', [routerController::class, 'paket'])->name('paket');
Route::get('/kursus', [routerController::class, 'kursus'])->name('kursus');