<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>MyBot - {{$title}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Lovus is responsive wedding html website template">
    <meta name="keywords" content="wedding,couple,ceremony,reception,rsvp,gallery,event,countdown">
    <meta name="author" content="">

    

    <!-- CSS Files HOME
    ================================================== -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="css/jquery.countdown.css" type="text/css">
    <link rel="stylesheet" href="css/stylehome.css" type="text/css">
    <link rel="stylesheet" href="css/animsition.min.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="css/color.css" type="text/css" id="colors">



    <!-- CSS Files SHOP
    ================================================== -->
    <link rel="icon" href="../image/mybotwhite.ico">
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/flaticon.css" />
    <link rel="stylesheet" href="css/owl.carousel.min.css" />
    <link rel="stylesheet" href="css/owlshop.theme.css" />
    <link rel="stylesheet" href="css/magnific-popupshop.css" />
    <link rel="stylesheet" href="css/lightgallery.css" />
    <link rel="stylesheet" href="css/woocommerce.css" />
    <link rel="stylesheet" href="css/royal-preload.css" />

    <link rel="stylesheet" href="css/styleshop.css" />

    <!-- CSS Files Langganan
    ================================================== -->
    <!-- CSS:: FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface" rel="stylesheet"> 

    <!-- CSS:: BOOTSTRAP -->  
    <link href="css/langganan/bootstrap.min.css" rel="stylesheet">
    <link href="css/langganan/all.min.css" rel="stylesheet">
    <link href="css/langganan/animate.css" rel="stylesheet">
    <link href="css/langganan/owl.carousel.min.css" rel="stylesheet">
    <link href="css/langganan/ekko-lightbox.css" rel="stylesheet">
    <link href="css/langganan/aos.min.css" rel="stylesheet">
    <link href="css/langganan/main.css" rel="stylesheet"> 

    

</head>

<body id="homepage">

    <div id="wrapper">

        <!-- header begin -->
        <header>


            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- logo begin -->
                        <div id="logo">
                            <a href="index.html">
                                <h2>MyBot</h2>
                            </a>
                        </div>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->

                        <a href="{{ route('login') }}"><span class="btn-rsvp">Log In</span></a>

                        <!-- mainmenu begin -->
                        <nav>
                            <ul id="mainmenu">
                                <li><a href="{{ route('dashboard') }}">E-Course</a></li>
                                <li><a href="{{ route('home') }}">Home</a>
                                </li>
                                <li><a href="{{ route('langganan') }}#">Paket Langganan</a>
                                </li>
                                <li><a href="{{ route('shop') }}">Shop</a></li>
                            </ul>
                        </nav>

                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
        <!-- header close -->

        <!-- Start #main -->
        <main id="mainhome">
            @yield('mainhome')
        </main> 
        <!-- End #main -->

        <!-- footer begin -->
        <footer>
            <div class="container text-center text-light">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="hs1 wow fadeInUp">
                            <span>MyBot</span> <br>
                            Buat Robot Sendiri, Bukan Mimpi!
                        </h2>
                    </div>
                </div>
            </div>

            <div class="subfooter">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-md-12">
                            &copy; Copyright 2021 - MyBot's Company
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer close -->

        <a href="#" id="back-to-top"></a>
        <div id="preloader">
            <div class="preloader1"></div>
        </div>
    </div>



    <!-- Javascript Files HOME.blade.php
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/easing.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/validation-rsvp.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/enquire.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/countdown-custom.js"></script>
    <script src="js/animsition.min.js"></script>
    <script src="js/designesia.js"></script>

    



</body>

</html>