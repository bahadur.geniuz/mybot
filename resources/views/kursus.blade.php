@extends('layout/mainlms')

@section('mainlms')

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container-full">
      <!-- Main content -->
      <section class="content">
          <div class="row">
              <div class="col-12">
                  <div class="box bg-gradient-primary overflow-hidden pull-up">
                      <div class="box-body pe-0 ps-lg-50 ps-15 py-0">							
                          <div class="row align-items-center">
                              <div class="col-12 col-lg-8">
                                  <h1 class="fs-40 text-white">Selamat Datang Alviantara!</h1>
                                  <p class="text-white mb-0 fs-20">
                                    Belajar membuat robot secara online lebih efektif lewat fitur interaktif dengan MyBot. Yuk, mulai chat sama Tutor dan Coach-mu!
                                  </p>
                              </div>
                              <div class="col-12 col-lg-4"><img src="../images/svg-icon/color-svg/custom-15.svg" alt=""></div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <!-- /.content -->
          <div class="row">
              <div class="col-xl-3 col-md-6 col-12">
                  <div class="box pull-up">
                      <div class="box-body">	
                          <div class="bg-primary rounded">
                              <h5 class="text-white text-center p-10">Mikrokontroler</h5>
                          </div>
                          <p class="mb-0 fs-18"><strong>Pembuatan Drone (H-1)</strong></p>
                          <p class="mb-0 fs-18">Melanjutkan pembuatan pengendalian sayap dengan servo tunggal</p>
                          <div class="d-flex justify-content-between mt-30">
                              <div>
                                  <p class="mb-0 text-fade">Tanggal</p>
                                  <p class="mb-0">Tempat</p>
                              </div>
                              <div>
                                  <p class="mb-5 fw-600">02 Nov</p>
                                  <p class="mb-5 fw-600">Singaraja</p>
                              </div>
                          </div>								
                      </div>					
                  </div>
              </div>
              <div class="col-xl-3 col-md-6 col-12">
                  <div class="box pull-up">
                      <div class="box-body">	
                          <div class="bg-warning rounded">
                              <h5 class="text-white text-center p-10">Konsep</h5>
                          </div>
                          <p class="mb-0 fs-18"><strong>State Mesin (H-1)</strong></p>
                          <p class="mb-0 fs-18">Melanjutkan Skema Penyelesaian Produk yang lebih Konstruktif</p>
                          <div class="d-flex justify-content-between mt-30">
                              <div>
                                  <p class="mb-0 text-fade">Tanggal</p>
                                  <p class="mb-0">Tempat</p>
                              </div>
                              <div>
                                  <p class="mb-5 fw-600">03 Nov</p>
                                  <p class="mb-5 fw-600">Singaraja</p>
                              </div>
                          </div>								
                      </div>					
                  </div>
              </div>
              <div class="col-xl-3 col-md-6 col-12">
                  <div class="box pull-up">
                      <div class="box-body">	
                          <div class="bg-danger rounded">
                              <h5 class="text-white text-center p-10">Project</h5>
                          </div>
                          
                          <p class="mb-0 fs-18"><strong>Bahasa C++ (H-2)</strong></p>
                          <p class="mb-0 fs-18">Melanjutkan implementasi method dan inheritance dalam pembuatan robot</p>
                          <div class="d-flex justify-content-between mt-30">
                              <div>
                                  <p class="mb-0 text-fade">Tanggal</p>
                                  <p class="mb-0">Tempat</p>
                              </div>
                              <div>
                                  <p class="mb-5 fw-600">09 Nov</p>
                                  <p class="mb-5 fw-600">Singaraja</p>
                              </div>
                          </div>								
                      </div>					
                  </div>
              </div>
              <div class="col-xl-3 col-md-6 col-12">
                  <div class="box pull-up">
                      <div class="box-body">	
                          <div class="bg-info rounded">
                              <h5 class="text-white text-center p-10">Programming</h5>
                          </div>
                          <p class="mb-0 fs-18"><strong>Robot Terbang (H-4)</strong></p>
                          <p class="mb-0 fs-18">Melanjutkan pembuatan pengendalian sayap dengan servo tunggal</p>
                          <div class="d-flex justify-content-between mt-30">
                            <div>
                                <p class="mb-0 text-fade">Tanggal</p>
                                <p class="mb-0">Tempat</p>
                            </div>
                            <div>
                                <p class="mb-5 fw-600">10 Nov</p>
                                <p class="mb-5 fw-600">Singaraja</p>
                            </div>
                         </div>								
                      </div>					
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-primary rounded">
                            <h5 class="text-white text-center p-10">Mikrokontroler</h5>
                        </div>
                        <p class="mb-0 fs-18"><strong>Pembuatan Drone (H-1)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan pembuatan pengendalian sayap dengan servo tunggal</p>
                        <div class="d-flex justify-content-between mt-30">
                            <div>
                                <p class="mb-0 text-fade">Tanggal</p>
                                <p class="mb-0">Tempat</p>
                            </div>
                            <div>
                                <p class="mb-5 fw-600">02 Nov</p>
                                <p class="mb-5 fw-600">Singaraja</p>
                            </div>
                        </div>								
                    </div>					
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-warning rounded">
                            <h5 class="text-white text-center p-10">Konsep</h5>
                        </div>
                        <p class="mb-0 fs-18"><strong>State Mesin (H-1)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan Skema Penyelesaian Produk yang lebih Konstruktif</p>
                        <div class="d-flex justify-content-between mt-30">
                            <div>
                                <p class="mb-0 text-fade">Tanggal</p>
                                <p class="mb-0">Tempat</p>
                            </div>
                            <div>
                                <p class="mb-5 fw-600">03 Nov</p>
                                <p class="mb-5 fw-600">Singaraja</p>
                            </div>
                        </div>								
                    </div>					
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-danger rounded">
                            <h5 class="text-white text-center p-10">Project</h5>
                        </div>
                        
                        <p class="mb-0 fs-18"><strong>Bahasa C++ (H-2)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan implementasi method dan inheritance dalam pembuatan robot</p>
                        <div class="d-flex justify-content-between mt-30">
                            <div>
                                <p class="mb-0 text-fade">Tanggal</p>
                                <p class="mb-0">Tempat</p>
                            </div>
                            <div>
                                <p class="mb-5 fw-600">09 Nov</p>
                                <p class="mb-5 fw-600">Singaraja</p>
                            </div>
                        </div>								
                    </div>					
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-info rounded">
                            <h5 class="text-white text-center p-10">Programming</h5>
                        </div>
                        <p class="mb-0 fs-18"><strong>Robot Terbang (H-4)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan pembuatan pengendalian sayap dengan servo tunggal</p>
                        <div class="d-flex justify-content-between mt-30">
                          <div>
                              <p class="mb-0 text-fade">Tanggal</p>
                              <p class="mb-0">Tempat</p>
                          </div>
                          <div>
                              <p class="mb-5 fw-600">10 Nov</p>
                              <p class="mb-5 fw-600">Singaraja</p>
                          </div>
                       </div>								
                    </div>					
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-primary rounded">
                            <h5 class="text-white text-center p-10">Mikrokontroler</h5>
                        </div>
                        <p class="mb-0 fs-18"><strong>Pembuatan Drone (H-1)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan pembuatan pengendalian sayap dengan servo tunggal</p>
                        <div class="d-flex justify-content-between mt-30">
                            <div>
                                <p class="mb-0 text-fade">Tanggal</p>
                                <p class="mb-0">Tempat</p>
                            </div>
                            <div>
                                <p class="mb-5 fw-600">02 Nov</p>
                                <p class="mb-5 fw-600">Singaraja</p>
                            </div>
                        </div>								
                    </div>					
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-warning rounded">
                            <h5 class="text-white text-center p-10">Konsep</h5>
                        </div>
                        <p class="mb-0 fs-18"><strong>State Mesin (H-1)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan Skema Penyelesaian Produk yang lebih Konstruktif</p>
                        <div class="d-flex justify-content-between mt-30">
                            <div>
                                <p class="mb-0 text-fade">Tanggal</p>
                                <p class="mb-0">Tempat</p>
                            </div>
                            <div>
                                <p class="mb-5 fw-600">03 Nov</p>
                                <p class="mb-5 fw-600">Singaraja</p>
                            </div>
                        </div>								
                    </div>					
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-danger rounded">
                            <h5 class="text-white text-center p-10">Project</h5>
                        </div>
                        
                        <p class="mb-0 fs-18"><strong>Bahasa C++ (H-2)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan implementasi method dan inheritance dalam pembuatan robot</p>
                        <div class="d-flex justify-content-between mt-30">
                            <div>
                                <p class="mb-0 text-fade">Tanggal</p>
                                <p class="mb-0">Tempat</p>
                            </div>
                            <div>
                                <p class="mb-5 fw-600">09 Nov</p>
                                <p class="mb-5 fw-600">Singaraja</p>
                            </div>
                        </div>								
                    </div>					
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12">
                <div class="box pull-up">
                    <div class="box-body">	
                        <div class="bg-info rounded">
                            <h5 class="text-white text-center p-10">Programming</h5>
                        </div>
                        <p class="mb-0 fs-18"><strong>Robot Terbang (H-4)</strong></p>
                        <p class="mb-0 fs-18">Melanjutkan pembuatan pengendalian sayap dengan servo tunggal</p>
                        <div class="d-flex justify-content-between mt-30">
                          <div>
                              <p class="mb-0 text-fade">Tanggal</p>
                              <p class="mb-0">Tempat</p>
                          </div>
                          <div>
                              <p class="mb-5 fw-600">10 Nov</p>
                              <p class="mb-5 fw-600">Singaraja</p>
                          </div>
                       </div>								
                    </div>					
                </div>
            </div>
        </div>



		
      </section>
      <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->

@endsection