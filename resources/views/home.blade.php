@extends('layout/mainhome')

@section('mainhome')

    <!-- content begin -->
    <div id="content" class="no-bottom no-top">

        

        <!-- section begin -->
        <section id="section-hero-1" class="full-height relative z1 owl-slide-wrapper no-top no-bottom text-light" data-stellar-background-ratio=".2">
            <div class="center-y fadeScroll relative" data-scroll-speed="4">
                <div class="container">
                    <div class="row">
                        <div class="spacer-double"></div>
                        <div class="col-md-4 col-md-offset-4">
                            <img src="image/mybotwhite.png" class="img-circle" alt="">
                            
                        </div>
                        <div class="row wow fadeInUp">
                            <div class="col-md-8 col-md-offset-2 ">
                                <div class="very-big text-light wow fadeIn">
                                    <strong> membantu anda dalam membuatan robot sendiri serta terhubung dalam materi robotika bersama mentor <br> dan rekan MyBot </strong>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->


        <!-- section begin -->
        <section id="section-couple">
            <div class="container relative z-index">
                <div class="row">
                    <div class="col-md-5 col-md-offset-1 text-center">
                        <div class="padding40">
                            <h2>Creation</h2>
                            <p>Belajar robotik bersama dengan mentor hingga dapat membuat robot pribadi</p>
                        </div>
                    </div>

                    <div class="col-md-5 text-center">
                        <div class="padding40">
                            <h2>Quick Way</h2>
                            <p>Belajar robotik didampingi saat pertemuan serta konsultasi tertutup dan terbuka dalam mengatasi berbagai permasalahan yang ada dalam pembelajaran
                            </p>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="row">
                    <div class="col-md-5 col-md-offset-1 text-center">
                        <div class="padding40">
                            <h2>Fleksibilitas</h2>
                            <p>Layanan pelatihan pembuatan robot secara online maupun offline (workshop). Belajar lebih seru bersama Super Teacher terbaik, banyak video belajar online serta ada game seru setelah menyimak video. </p>
                        </div>
                    </div>

                    <div class="col-md-5 text-center">
                        <div class="padding40">
                            <h2>Verified</h2>
                            <p>Modul pembelajaran yang interaktif, dengan animasi dan materi yang to the point. Pastinya dengan isi materi yang lengkap dan bermanfaat.</p>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="section-quote" aria-label="section-quote-1" class="text-light" data-stellar-background-ratio=".2" data-bgcolor="#f8578d">
            <div class="container">
                <div class="row wow fadeInUp">
                    <div class="col-md-8 col-md-offset-2">
                        <blockquote class="very-big text-light wow fadeIn">
                            <span>Kini Menang Dalam Kompetisi Robot <br>
                            Bukan Hanya Mimpi<br>

                            Mari Berlangganan MyBot<br>

                            Belajar Mandiri
                            Penuh Inovasi
                            </span>
                        </blockquote>
                    </div>
                </div>
            </div>

           
        </section>
        <!-- section close -->


        <!-- section begin -->
        <section id="section-gallery" aria-label="section-gallery" class="no-top no-bottom">
            <div id="gallery-carousel-4" class="owl-slide">

                <!-- gallery carousel-item -->
                <div class="carousel-item">
                    <figure class="picframe">
                        <a class="image-popup" href="images/gallery/category-1/1.jpg">
                        <span class="overlay-v">
                        <i></i>
                        </span>
                    </a>
                        <img src="images/gallery/category-1/1.jpg" class="img-responsive" alt="">
                    </figure>
                </div>
                <!-- close gallery carousel-item -->

                <!-- gallery carousel-item -->
                <div class="carousel-item">
                    <figure class="picframe">
                        <a class="image-popup" href="images/gallery/category-1/2.jpg">
                        <span class="overlay-v">
                        <i></i>
                        </span>
                    </a>
                        <img src="images/gallery/category-1/2.jpg" class="img-responsive" alt="">
                    </figure>
                </div>
                <!-- close gallery carousel-item -->

                <!-- gallery carousel-item -->
                <div class="carousel-item">
                    <figure class="picframe">
                        <a class="image-popup" href="images/gallery/category-1/3.jpg">
                        <span class="overlay-v">
                        <i></i>
                        </span>
                    </a>
                        <img src="images/gallery/category-1/3.jpg" class="img-responsive" alt="">
                    </figure>
                </div>
                <!-- close gallery carousel-item -->

                <!-- gallery carousel-item -->
                <div class="carousel-item">
                    <figure class="picframe">
                        <a class="image-popup" href="images/gallery/category-1/4.jpg">
                        <span class="overlay-v">
                        <i></i>
                        </span>
                    </a>
                        <img src="images/gallery/category-1/4.jpg" class="img-responsive" alt="">
                    </figure>
                </div>
                <!-- close gallery carousel-item -->

                <!-- gallery carousel-item -->
                <div class="carousel-item">
                    <figure class="picframe">
                        <a class="image-popup" href="images/gallery/category-1/5.jpg">
                        <span class="overlay-v">
                        <i></i>
                        </span>
                    </a>
                        <img src="images/gallery/category-1/5.jpg" class="img-responsive" alt="">
                    </figure>
                </div>
                <!-- close gallery carousel-item -->

                <!-- gallery carousel-item -->
                <div class="carousel-item">
                    <figure class="picframe">
                        <a class="image-popup" href="images/gallery/category-1/6.jpg">
                        <span class="overlay-v">
                        <i></i>
                        </span>
                    </a>
                        <img src="images/gallery/category-1/6.jpg" class="img-responsive" alt="">
                    </figure>
                </div>
                <!-- close gallery carousel-item -->

            </div>
        </section>
        <!-- section close -->

        <!-- section begin -->
        <section aria-label="section" class="no-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="deco id-color"><span>AKTIVITAS DI BULAN INI</span></h2>
                        <h2 data-wow-delay=".2s">NOVEMBER 2021</h2>
                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="section-event">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <img src="image/home/plane.jpg" alt="" class="img-responsive img-rounded wow fadeInLeft">
                    </div>

                    <div class="col-md-5 col-md-offset-1 pt40 pb40 wow fadeIn" data-wow-delay=".5s">
                        <h3>Pembuatan Robot Pesawat Terbang</h3>
                        13-15 November 2021<br> 13:00 - 16:00 Wita<br> Ganesha 2, Singaraja, Bali<br>
                        <a href="https://goo.gl/maps/ih6Nayw3DnHnxYwc8"
                            class="btn btn-custom mt30 popup-gmaps">Lihat Lokasi</a>
                    </div>
                </div>

                <div class="spacer-double"></div>

                <div class="row">
                    <div class="col-md-5 pt40 pb40 text-right wow fadeIn" data-wow-delay=".5s">
                        <h3>Mini Arduino Building</h3>
                        23-25 November 2021<br> 14:00 PM - 17:00 PM<br> SMAN 1 Singaraja, Singaraja, Bali<br>
                        <a href="https://goo.gl/maps/o7AGTSWtEwJPfgE68"
                            class="btn btn-custom mt30 popup-gmaps">Lihat Lokasi</a>
                    </div>

                    <div class="col-md-6 col-md-offset-1">
                        <img src="image/home/drone.jpg" alt="" class="img-responsive img-rounded wow fadeInRight">
                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->

        <!-- section begin -->
        <section id="section-guestbook" class="text-light" data-stellar-background-ratio=".2">
            <div class="container relative">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <div class="very-big text-light wow fadeIn">
                            <strong> Be MyBot</strong>
                        </div>
                        <div class="spacer-single"></div>
                    </div>


                    <div id="testimonial-carousel" class="de_carousel" data-wow-delay=".3s">

                        <div class="col-md-10 item ">
                            <div class="de_testi opt-4">
                                <blockquote>
                                    <p>Belajar Robotik Online Bersama Mentor 
                                        Terbaik, dilengkapi Video Pembelajaran dan modul belajar Serta Banyak Projek Robot dan Game yang Asyik setelah Pembelajaran.</p>
                                    <div class="de_testi_by">
                                        E-COURSE
                                    </div>
                                </blockquote>

                            </div>
                        </div>

                        <div class="col-md-10 item">
                            <div class="de_testi opt-4">
                                <blockquote>
                                    <p>Layanan pelatihan pembuatan robot secara online maupun offline (workshop) didampingi oleh mentor berpengalaman yang pastinya akan seru dan asyik. Banyak jenis robot yang dapat dipilih dan  untuk masa percobaan nikmati semuanya secara GRATIS !</p>
                                    <div class="de_testi_by">
                                        WORKSHOP ON-OFFLINE
                                    </div>
                                </blockquote>
                            </div>
                        </div>

                        <div class="col-md-10 item">
                            <div class="de_testi opt-4">
                                <blockquote>
                                    <p>Layanan konsultasi dan pendampingan  merupakan proses pembelajaran yang dimana peserta dapat berkonsultasi dengan didampingi oleh mentor berpengalaman yang berkompeten di bidangnya.</p>
                                    <div class="de_testi_by">
                                        KONSULTASI DAN PENDAMPINGAN
                                    </div>
                                </blockquote>
                            </div>
                        </div>

                        <div class="col-md-10 item">
                            <div class="de_testi opt-4">
                                <blockquote>
                                    <p>Belanja di ToNik (Toko Elektronik) powered by
                                        MyBot akan memudahkan dalam mencari komponen 
                                        kualitas baik, dengan harga yang murah, serta 
                                        sudah disediakan banyak paketan project,
                                        sehingga memudahkan dalam proses perakitan robot.</p>
                                    <div class="de_testi_by">
                                        TONIK (TOKO ELEKTRONIK)
                                    </div>
                                </blockquote>
                            </div>
                        </div>

                        <div class="col-md-10 item">
                            <div class="de_testi opt-4">
                                <blockquote>
                                    <p>Modul pembelajaran yang interaktif, dengan
                                        animasi dan materi yang to the point. 
                                        Pastinya dengan isi materi yang lengkap dan bermanfaat.</p>
                                    <div class="de_testi_by">
                                        MEDIA PEMBELAJARAN INTERAKTIF
                                    </div>
                                </blockquote>
                            </div>
                        </div>

                        <div class="col-md-10 item">
                            <div class="de_testi opt-4">
                                <blockquote>
                                    <p>Mencari komponen yang cocok bagi suatu projek akan mudah, karena banyak projek yang tersedia, serta ada banyak variasi pemrograman. Mencari referensi dan ilmu akan sangat dimudahkan dengan Modul Pembelajaran dari MyBot</p>
                                    <div class="de_testi_by">
                                        VARIATIF
                                    </div>
                                </blockquote>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </section>
        <!-- section close -->

    </div>
    <!-- content close -->

    <!-- Javascript Files HOME.blade.php
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/easing.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/validation-rsvp.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/enquire.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/countdown-custom.js"></script>
    <script src="js/animsition.min.js"></script>
    <script src="js/designesia.js"></script>
@endsection