@extends('layout/mainhome')

@section('mainhome')

    <section id="section-video" class="full-height no-padding text-light" data-speed="5" data-type="background" aria-label="section-video">
        <div class="de-video-container">
            <div class="de-video-content">
                <div class="center-y fadeScroll text-center relative" data-scroll-speed="2">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="very-big text-light wow fadeIn">
                                    <strong> Layanan pelatihan pembuatan robot secara online maupun offline (workshop) didampingi oleh mentor berpengalaman hingga dapat membuat robot pribadi. Buat Robot Bukan Mimpi! </strong>
                                </div>
                                <div class="spacer-double"></div>
                                <div class="spacer-double"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="de-video-overlay"></div>

            <!-- load your video here -->
            <video autoplay="" loop="" muted="" poster="images/zzpaket/video-poster.jpg">
                <source src="image/robot.mp4" type="video/mp4" />
            </video>

        </div>

    </section>

    <section style="background-image: url('../image/black.jpg');">
        <div class="p-page_inner packages_box_cover">
        <div class="p-section_head">
            <h2>Paket Langganan</h2>
        </div>
        <div class="container p-content_section">
            <div class="row clearfix p-packages">
            

            <div class="col-md-4 p-package_outer deluxe_package_outer">
                <div class="p-single_package">
                <div class="p-package_head">
                    <img src="image/pattern.png" class="img-fluid" alt="">
                    <div class="p-pack_head_text">
                    <div class="p-package_title">
                        <span>Beginner Package</span>
                    </div>
                    <div class="p-pack_head_text">
                        <div class="p-title_strip">
                        <span><i class="fa fa-clock-o"></i> Satu Bulan</span>
                        <span class="p-package_money"><i class="fa fa-usd"></i>Rp. 250.000</span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="p-package_content">
                    <ul class="list_box">
                    
                    <li>Text</li>
                    <li>Text</li>
                    <li>Text</li>
                    <li>Text</li>
                    <li>Text</li>
                    <li>Text</li>
                    <li>Text</li>
                    <li>Text</li>
                    
                    </ul> 
                    <a class="offer_btn" href="contact.html">Pembelian</a>
                </div>
                </div>
            </div> 


            <div class="col-md-4 p-package_outer gold_package_outer">
                <div class="p-single_package">
                <div class="p-package_head">
                    <img src="image/pattern.png" class="img-fluid" alt="">
                    <div class="p-pack_head_text">
                    <div class="p-package_title">
                        <span>Expert Package</span>
                    </div>
                    <div class="p-pack_head_text"> 
                        <div class="p-title_strip">
                        <span><i class="fa fa-clock-o"></i> Satu Bulan</span>
                        <span class="p-package_money"><i class="fa fa-usd"></i> Rp. 500.000</span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="p-package_content">
                    <ul class="list_box">
                    
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                    
                    </ul>  
                    <a class="offer_btn" href="contact.html">Pembelian</a>

                </div>
                </div>
            </div>



            <div class="col-md-4 p-package_outer signature_package_outer">
                <div class="p-single_package">
                <div class="p-package_head">
                    <img src="image/pattern.png" class="img-fluid" alt="">
                    <div class="p-pack_head_text">
                    <div class="p-package_title">
                        <span>Intermediet Package</span>
                    </div>
                    <div class="p-pack_head_text">
                        <div class="p-title_strip">
                        <span><i class="fa fa-clock-o"></i> Satu Bulan</span>
                        <span class="p-package_money"><i class="fa fa-usd"></i> Rp 350.000</span>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="p-package_content">
                    <ul class="list_box">
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                        <li>Text</li>
                    </ul> 
                    <a class="offer_btn" href="contact.html">Pembelian</a>

                </div>
                </div>
            </div> 


            </div>
        </div>
        </div>
    </section> 


@endsection