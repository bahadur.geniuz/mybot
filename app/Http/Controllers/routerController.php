<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class routerController extends Controller
{
    public function home()
    {
        return view('home', [
            "title" => "Home"
        ]);
    }
    
    public function welcome()
    {
        return view('welcome', [
            "title" => "Welcome"
        ]);
    }
    
    public function dashboard()
    {
        return view('dashboard', [
            "title" => "Dashboard"
        ]);
    }

    public function register()
    {
        return view('register', [
            "title" => "Register"
        ]);
    }

    public function login()
    {
        return view('login', [
            "title" => "Login"
        ]);
    }

    public function chat()
    {
        return view('chat', [
            "title" => "Chat"
        ]);
    }

    public function shop()
    {
        return view('shop', [
            "title" => "Shop"
        ]);
    }

    public function langganan()
    {
        return view('langganan', [
            "title" => "Langganan"
        ]);
    }

    public function paket()
    {
        return view('paket', [
            "title" => "Paket"
        ]);
    }

    public function kursus()
    {
        return view('kursus',[
            "title" => "Kursus"
        ])->name('kursus');;
    }

    

    

}